from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings

from django.contrib.auth.decorators import login_required

from . import views


urlpatterns = [
    url(r'^$', views.PostListView.as_view(), name="post_list"),
    url(r'^draft/$', login_required(views.DraftsPageView.as_view()), name="post_draft"),
    url(r'^post/(?P<pk>[\d+])/$', views.PostDetailView.as_view(), name="post_detail"),
    url(r'^post/(?P<pk>[\d+])/edit/$', login_required(views.PostEditView.as_view()), name="post_edit"),
    url(r'^post/(?P<pk>[\d+])/delete/$', login_required(views.PostDeleteView.as_view()), name="post_delete"),
    url(r'^post/(?P<pk>[\d+])/publish/$', login_required(views.PostPublishView.as_view()), name="post_publish"),
    url(r'^post/(?P<pk>[\d+])/comment/$', views.AddCommentView.as_view(), name="add_comment"),
    url(r'^post/new/$', login_required(views.PostNewView.as_view()), name="post_new"),
    url(r'^comment/(?P<pk>[\d+])/approve/$', login_required(views.CommentApproveView.as_view()), name="comment_approve"),
    url(r'^comment/(?P<pk>[\d+])/remove/$', login_required(views.CommentRemoveView.as_view()), name="comment_remove"),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
