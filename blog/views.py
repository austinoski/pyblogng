from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy, reverse

from django.views import View

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView

from .models import Post, Comment


class PostListView(ListView):
    model = Post
    context_object_name = "posts"
    template_name = "blog/index.html"

    def get_queryset(self):
        queryset = super(PostListView, self).get_queryset()
        queryset = queryset.filter(published_date__isnull=False)
        return queryset


class DraftsPageView(ListView):
    model = Post
    template_name = "blog/draft.html"
    context_object_name = "posts"

    def get_queryset(self):
        queryset = super(DraftsPageView, self).get_queryset()
        queryset = queryset.filter(published_date__isnull=True).order_by('create_date')
        return queryset


class PostDetailView(DetailView):
    model = Post
    context_object_name = "post"
    template_name = "blog/detail.html"


class PostNewView(CreateView):
    template_name = "blog/new.html"
    model = Post
    fields = ['title', 'text']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super(PostNewView, self).form_valid(form)


class PostEditView(UpdateView):
    template_name = "blog/edit.html"
    model = Post
    fields = ['title', 'text']

    def form_valid(self, form):
        form.instance.save()
        return super(PostEditView, self).form_valid(form)


class PostDeleteView(DeleteView):
    template_name = "blog/confirm_delete.html"
    model = Post
    success_url = reverse_lazy('post_list')


class PostPublishView(View):
    def get(self, request, *args, **kwargs):
        post = get_object_or_404(Post, pk=kwargs['pk'])
        post.publish()
        return redirect(reverse('post_list'))


class AddCommentView(CreateView):
    template_name = "blog/new_comment.html"
    model = Comment
    fields = ['author', 'text']

    def form_valid(self, form):
        post = get_object_or_404(Post, pk=self.kwargs['pk'])
        form.instance.post = post
        return super(AddCommentView, self).form_valid(form)


class CommentRemoveView(DeleteView):
    template_name = "blog/confirm_comment_delete.html"
    model = Comment
    success_url = reverse_lazy('post_list')


class CommentApproveView(View):
    def get(self, request, *args, **kwargs):
        comment = get_object_or_404(Comment, pk=self.kwargs['pk'])
        comment.approve()
        return redirect(comment.post.get_absolute_url())
