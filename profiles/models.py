from django.db import models
from django.contrib.auth.user import User



class Profile(models.Model):
    user = models.ForeignField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.username
