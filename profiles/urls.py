from django.conf.urls import url


urlpatterns = [
    url("^user/(?P<pk>[\d+])", views.ProfileView.as_view(), name="profile"),
]